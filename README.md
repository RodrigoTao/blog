# README #

### Programming Assignment 1 ###

* Programming Assignment 1
* Rodrigo Taocuitli Cortés Cuevas
* Version 1.0
* https://RodrigoTao@bitbucket.org/RodrigoTao/blog.git

### Assignment 1 Instructions ###

For this assignment, students will be setting up their BitBucket accounts and pushing the Rails “blog” application to a repository on BitBucket. (For instructions on how to use Git to accomplish this, please refer to the posted instructions)

*For help and more in-depth instructions on this assignment, please refer to the Module 2 Lecture 3 Video.
* This corresponds to iteration 1 of the blog app in the lecture videos.

Please make sure to use the same names used in the videos for all Rails variables, classes, etc.. The grading scheme depends on you following the exact instructions provided in the videos.

      1. Create a BitBucket account
          You will need to create a BitBucket account at bitbucket.org.

       2. Create the “Blog” application using Ruby on Rails
          Create a rails project, using the “new” command. This project should be named “blog”.
          You will need to scaffold tables for posts and comments. The posts table should have the following two columns: “title”
          of type string and “body” of type text. The comments table will also have two columns as follows: “post_id” of type 
          integer and “body” of type text.

      3. Push the “Blog” application to BitBucket
          Create a repository on BitBucket (be sure this repository is public). Then push your Rails project to this repository. Refer 
          to the git instructions posted for help with this step. Finally, follow the instructions on the assignment page to submit 
          your Bitbucket URL to the automated grader queue.

